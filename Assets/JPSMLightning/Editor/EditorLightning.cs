﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

[CustomEditor(typeof(Lightning))]
public class EditorLightning : Editor
{
    private int activeTab = 0;

    public override void OnInspectorGUI()
    {
        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Targets")) activeTab = 0;
        if (GUILayout.Button("Settings")) activeTab = 1;
        GUILayout.EndHorizontal();

        GUILayout.Space(20);
         
        Lightning lightning = target as Lightning;

        if (activeTab == 0)
        {
            int i = 0;
            foreach (Vector3 t in lightning.targets)
            {
                GUILayout.BeginHorizontal();
                lightning.targets[i] = EditorGUILayout.Vector3Field("", t);
                if (GUILayout.Button("X"))
                {
                    List<Vector3> old = new List<Vector3>();
                    old.AddRange(lightning.targets);
                    old.RemoveAt(i);
                    lightning.targets = old.ToArray();
                    break;
                }
                GUILayout.EndHorizontal();

                i++;
            }

            if (i > 0) GUILayout.Space(3); 
            if (GUILayout.Button("Add", GUILayout.Width(60f)))
            {
                List<Vector3> old = new List<Vector3>();
                old.AddRange(lightning.targets);
                old.Add(Vector3.zero);
                lightning.targets = old.ToArray();
            }
        }
        else
        {
            lightning.lineCount = EditorGUILayout.IntSlider("Rays", lightning.lineCount, 1, 5);
            GUILayout.Space(10);

            lightning.lineWidth = (Lightning.WidthType)EditorGUILayout.EnumPopup("Ray width", lightning.lineWidth);
            switch (lightning.lineWidth)
            {
                case Lightning.WidthType.CURVE:
                    lightning.widthCurve = EditorGUILayout.CurveField( lightning.widthCurve);
                    break;

                case Lightning.WidthType.FIXED:
                    lightning.widthFixed = EditorGUILayout.Slider(lightning.widthFixed, 0.5f, 3f);
                    break;

                case Lightning.WidthType.RANDOM:
                    EditorGUILayout.MinMaxSlider(ref lightning.widthMin, ref lightning.widthMax, 0.5f, 3f);
            		EditorGUILayout.LabelField("Min Val:", lightning.widthMin.ToString());
                    EditorGUILayout.LabelField("Max Val:", lightning.widthMax.ToString());
                    break;
            }
        }
    }
}