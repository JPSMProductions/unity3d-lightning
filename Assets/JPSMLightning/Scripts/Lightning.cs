﻿using UnityEngine;

public class Lightning : MonoBehaviour
{
    #region PUBLIC_MEMBER_VAR

    public Vector3[] targets;

    public WidthType lineWidth;
    public float widthFixed;
    public float widthMin, widthMax;
    public AnimationCurve widthCurve;

    public int lineCount;

    #endregion

    #region ENUMS

    public enum WidthType
    {
        FIXED, CURVE, RANDOM
    }

    #endregion
}
